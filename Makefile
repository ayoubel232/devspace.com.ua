.PHONY: up app down view view-leaflet view-design

install:
	sudo docker-compose up -d --build
	sudo docker exec devspace_frontend npm install

up:
	sudo docker-compose up -d

app:
	sudo docker exec -it devspace_frontend bash

down:
	sudo docker-compose down

build:
	sudo docker-compose up -d
	sudo docker exec devspace_frontend npm run build

view:
	google-chrome public/index.html

view-leaflet:
	google-chrome public/leaflet.html

view-design:
	google-chrome design/v2/index_2.html

view-design-i18n:
	google-chrome design/v3/index_3.html